/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2018 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/* inclusion guard */

#ifndef __AUM_BOOT_STATE_EFI_H__
#define __AUM_BOOT_STATE_EFI_H__

#include "boot-state.h"

G_BEGIN_DECLS

#define AUM_TYPE_BOOT_STATE_EFI (aum_boot_state_efi_get_type())

G_DECLARE_FINAL_TYPE(AumBootStateEfi,
                     aum_boot_state_efi,
                     AUM,
                     BOOT_STATE_EFI,
                     GObject);
G_END_DECLS
#endif /* __AUM_BOOT_STATE_EFI_H__ */

                                                                     
