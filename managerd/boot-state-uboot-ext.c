/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2018 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/**
 * SECTION: bootstateubootext
 * @title: AumBootStateUbootExt
 * @short_description: Ostree upgrade handler
 * @include: boot-state.h
 *
 * The boot state is responsible for reseting the bootcount
 */

#include "boot-state-uboot-ext.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zlib.h>
#include <errno.h>

/* For u-boot */
#define BC_FILE  "/boot/uboot.cnt"
#define BC_MAGIC_LEGACY 0xbc
#define BC_MAGIC 0xbd
#define BC_VERSION 1

struct _AumBootStateUbootExt
{
  GObject parent;
  gint active;
  gchar *filename;
  int bootcount;
  guint bootlimit;
  gboolean update_available;
  guint format_version;
};

typedef struct {
	gchar magic;
	gchar version;
	gchar bootcount;
	gchar upgrade_available;
} bootcount_ext_t;

static void
aum_boot_state_uboot_ext_iface_init (AumBootStateInterface *iface);

G_DEFINE_TYPE_WITH_CODE (AumBootStateUbootExt, aum_boot_state_uboot_ext, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (AUM_TYPE_BOOT_STATE, aum_boot_state_uboot_ext_iface_init));

/**
 * @brief Destroy environment structure
 *
 * @param state
 */
static void
aum_boot_state_uboot_ext_cleanup(AumBootStateUbootExt *state)
{
  g_clear_pointer(&state->filename, g_free);
}

/**
 * @brief Read bootcount from /boot/uboot.cnt
 *
 * @param state
 *
 * @return
 */
static gboolean
aum_boot_state_uboot_ext_read(AumBootStateUbootExt *state)
{
  g_autofree gchar *data = NULL;
  bootcount_ext_t *contents = NULL;
  gsize length = 0;
  g_autoptr (GError) error=NULL;

  g_file_get_contents (state->filename, &data, &length, &error);
  if (error)
    goto err;

  switch (length) {
    case 2:
      if (data[0] != BC_MAGIC_LEGACY)
        goto err;
      state->bootcount = data[1];
      state->format_version = 0;
      break;

    case sizeof(bootcount_ext_t):
      contents = (bootcount_ext_t *)data;

      if (contents->magic != BC_MAGIC || contents->version != BC_VERSION)
        goto err;

      state->bootcount = contents->bootcount;
      state->update_available = contents->upgrade_available;
      state->format_version = contents->version;
      break;

    default:
      goto err;
  }

  return TRUE;

err:
  g_message ("%s: %s", __FUNCTION__, error ? error->message : "unknown error");
  return FALSE;
}

/**
 * @brief save bootcount to disk
 *
 * @param state
 *
 * @return
 */
static gboolean
aum_boot_state_uboot_ext_save(AumBootStateUbootExt *state)
{
  g_autofree gchar *buffer;
  gsize length;
  g_autoptr (GError) error=NULL;

  switch (state->format_version) {
    case 0:
      length = 2;
      buffer = (gchar *) g_malloc0(length);
      if (buffer == NULL)
        goto err;

      buffer[0] = BC_MAGIC_LEGACY;
      buffer[1] = state->bootcount & 0xFF;
      break;

    case BC_VERSION:
      length = sizeof(bootcount_ext_t);
      buffer = (gchar *) g_malloc0(length);
      if (buffer == NULL)
        goto err;

      bootcount_ext_t *data = (bootcount_ext_t *) buffer;
      data->magic = BC_MAGIC;
      data->version = BC_VERSION;
      data->bootcount = state->bootcount & 0xFF;
      data->upgrade_available = state->update_available & 0xFF;
      break;

    default:
        goto err;
  }

  g_file_set_contents (state->filename, buffer, length, &error);
  if (error)
    goto err;

  return TRUE;

err:
  g_critical ("%s: %s", __FUNCTION__, error ? error->message : "unknown error");
  return FALSE;
}

static gboolean
aum_boot_state_uboot_ext_is_active (AumBootState * boot_state)
{
  AumBootStateUbootExt *state = AUM_BOOT_STATE_UBOOT_EXT(boot_state);
  return state->active;
}

static const gchar *
aum_boot_state_uboot_ext_get_name (AumBootState * boot_state)
{
  return "UBoot environment external";
}

static void
aum_boot_state_uboot_ext_init (AumBootStateUbootExt * state)
{
  state->filename = g_strdup(BC_FILE);
  state->active = FALSE;
  state->format_version = 0;

  if (aum_boot_state_uboot_ext_read(state) == FALSE)
    goto err;

  state->active = TRUE;

  g_message ("Loaded boot count from ext: count %d",
             state->bootcount);
  return;

err:
  g_message ("Cannot initialize boot-state from U-Boot ext environment");
}

static gint
aum_boot_state_uboot_ext_get_boot_count (AumBootState * boot_state)
{
  AumBootStateUbootExt *state = AUM_BOOT_STATE_UBOOT_EXT (boot_state);
  return state->bootcount;
}

static gint
aum_boot_state_uboot_ext_get_boot_limit (AumBootState * boot_state)
{
  AumBootStateUbootExt *state = AUM_BOOT_STATE_UBOOT_EXT (boot_state);
  return  state->bootlimit;
}

static void
aum_boot_state_uboot_ext_set_boot_limit (AumBootState * boot_state, guint limit)
{
  AumBootStateUbootExt *state = AUM_BOOT_STATE_UBOOT_EXT (boot_state);
  state->bootlimit = limit;
}

static gboolean
aum_boot_state_uboot_ext_get_update_available (AumBootState * boot_state)
{
  AumBootStateUbootExt *state = AUM_BOOT_STATE_UBOOT_EXT (boot_state);

  if (state->format_version == 0)
    return TRUE;

  return state->update_available;
}

/**
 * @brief Trigger bootcount mechanism in U_Boot
 *
 * @param boot_state
 * @param update_available: set to true if an update has just been applied, and before rebooting, or false if the reboot was successful
 *
 * @return
 */
static gboolean
aum_boot_state_uboot_ext_set_update_available (AumBootState * boot_state,
                                               gboolean update_available)
{
  AumBootStateUbootExt *state = AUM_BOOT_STATE_UBOOT_EXT (boot_state);
  state->bootcount = 0;
  state->update_available = update_available;
  return aum_boot_state_uboot_ext_save (state);
}

static void
aum_boot_state_uboot_ext_iface_init (AumBootStateInterface *iface)
{
  iface->is_active = aum_boot_state_uboot_ext_is_active;
  iface->get_name = aum_boot_state_uboot_ext_get_name;
  iface->get_boot_count = aum_boot_state_uboot_ext_get_boot_count;
  iface->get_boot_limit = aum_boot_state_uboot_ext_get_boot_limit;
  iface->set_boot_limit = aum_boot_state_uboot_ext_set_boot_limit;
  iface->get_update_available = aum_boot_state_uboot_ext_get_update_available;
  iface->set_update_available = aum_boot_state_uboot_ext_set_update_available;
}

static void
aum_boot_state_uboot_ext_dispose(GObject *obj)
{
  AumBootStateUbootExt *state = AUM_BOOT_STATE_UBOOT_EXT (obj);
  aum_boot_state_uboot_ext_cleanup(state);
}

static void
aum_boot_state_uboot_ext_class_init (AumBootStateUbootExtClass * cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);
  object_class->dispose = aum_boot_state_uboot_ext_dispose;
}
