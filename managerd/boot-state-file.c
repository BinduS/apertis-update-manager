/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2018 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/**
 * SECTION: bootstatefile
 * @title: AumBootStateFile
 * @short_description: Ostree upgrade handler
 * @include: boot-state.h
 *
 * The boot state is responsible for reseting the bootcount
 */

#include "boot-state-file.h"

#include <errno.h>

struct _AumBootStateFile
{
  GObject parent;
  const gchar *path;
  gint active;
  gint count;
  gint limit;
  gboolean update_available;
};

static void
aum_boot_state_file_iface_init (AumBootStateInterface *iface);

G_DEFINE_TYPE_WITH_CODE (AumBootStateFile, aum_boot_state_file, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (AUM_TYPE_BOOT_STATE, aum_boot_state_file_iface_init));

static gboolean
aum_boot_state_file_is_active (AumBootState * boot_state)
{
  AumBootStateFile *self = AUM_BOOT_STATE_FILE (boot_state);
  return self->active;
}

static const gchar *
aum_boot_state_file_get_name (AumBootState * boot_state)
{
  return "file";
}

static gboolean
aum_boot_state_file_update_on_disk (AumBootStateFile * self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GKeyFile) key_file = g_key_file_new ();

  if (!self->active)
    goto err;

  if (!g_key_file_load_from_file (key_file, self->path,
                                  G_KEY_FILE_NONE, &error))
    goto err;

  g_key_file_set_integer (key_file, "Boot", "Count", self->count);
  g_key_file_set_integer (key_file, "Boot", "Limit", self->limit);
  g_key_file_set_boolean (key_file, "Boot", "UpdateAvailable",
                                                       self->update_available);

  if (!g_key_file_save_to_file (key_file, self->path, &error))
    goto err;

  g_message ("Saved boot state: count %d, limit %d, update_available %d",
             self->count, self->limit, self->update_available);

  return TRUE;

err:
  g_message ("%s: %s", __FUNCTION__, error ? error->message : "unknown error");
  return FALSE;
}

static gint
aum_boot_state_file_get_boot_count (AumBootState * boot_state)
{
  AumBootStateFile *self = AUM_BOOT_STATE_FILE (boot_state);
  return self->count;
}

static gint
aum_boot_state_file_get_boot_limit (AumBootState * boot_state)
{
  AumBootStateFile *self = AUM_BOOT_STATE_FILE (boot_state);
  return self->limit;
}

static gboolean
aum_boot_state_file_get_update_available (AumBootState * boot_state)
{
  AumBootStateFile *self = AUM_BOOT_STATE_FILE (boot_state);
  return self->update_available;
}

static gboolean
aum_boot_state_file_set_update_available (AumBootState * boot_state,
                                          gboolean update_available)
{
  AumBootStateFile *self = AUM_BOOT_STATE_FILE (boot_state);
  self->update_available = update_available;
  self->count = 0;

  aum_boot_state_file_update_on_disk (self);

  return self->update_available;
}

static void
aum_boot_state_file_init (AumBootStateFile * self)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (GKeyFile) key_file = g_key_file_new ();

  self->path = "/etc/boot-state.ini";

  key_file = g_key_file_new ();
  if (!g_key_file_load_from_file (key_file, self->path, G_KEY_FILE_NONE, &error))
    goto err;

  self->update_available = g_key_file_get_boolean (key_file, "Boot", "UpdateAvailable", &error);
  if (error)
    goto err;

  self->count = g_key_file_get_integer (key_file, "Boot", "Count", &error);
  if (error)
    goto err;

  self->limit = g_key_file_get_integer (key_file, "Boot", "Limit", &error);
  if (error)
    goto err;

  self->active = TRUE;

  g_message ("Loaded boot state: count %d, limit %d, update_available %d",
             self->count, self->limit, self->update_available);

  return;

err:
  g_message ("Cannot initialize boot-state from file: %s", error->message);
  self->active = FALSE;
  self->count = 0;
  self->limit = 4;
}

static void
aum_boot_state_file_iface_init (AumBootStateInterface *iface)
{
  
  iface->is_active = aum_boot_state_file_is_active;
  iface->get_name = aum_boot_state_file_get_name;
  iface->get_boot_count = aum_boot_state_file_get_boot_count;
  iface->get_boot_limit = aum_boot_state_file_get_boot_limit;
  iface->get_update_available = aum_boot_state_file_get_update_available;
  iface->set_update_available = aum_boot_state_file_set_update_available;
}

static void
aum_boot_state_file_class_init (AumBootStateFileClass * cls)
{
}
