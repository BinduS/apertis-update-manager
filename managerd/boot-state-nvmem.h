/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2018 Collabora Ltd.
 * Copyright © 2020 Robert Bosch Power Tools GmbH
 *
 * Derived from boot-state-file.c
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/* inclusion guard */
#ifndef __AUM_BOOT_STATE_NVMEM_H__
#define __AUM_BOOT_STATE_NVMEM_H__

#include "boot-state.h"

G_BEGIN_DECLS

#define AUM_TYPE_BOOT_STATE_NVMEM (aum_boot_state_nvmem_get_type ())

G_DECLARE_FINAL_TYPE (AumBootStateNvmem,
                      aum_boot_state_nvmem,
                      AUM,
                      BOOT_STATE_NVMEM,
                      GObject);

G_END_DECLS

#endif /* __AUM_BOOT_STATE_NVMEM_H__ */
