/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2018 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/**
 * SECTION: bootstateefi
 * @title: AumBootStateEfi
 * @short_description: Ostree upgrade handler
 * @include: boot-state.h
 *
 * The boot state is responsible for renaming the conf file for efi based system
 */
#include "boot-state-efi.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <gio/gio.h>

/* For EFI */
#define DIR_NAME  "/boot/efi/loader/entries/"

struct _AumBootStateEfi
{
  GObject parent;
  gint active;
  gint bootcount;
  guint bootlimit;
  gboolean update_available;
};

static void
aum_boot_state_efi_iface_init (AumBootStateInterface *iface);

G_DEFINE_TYPE_WITH_CODE (AumBootStateEfi, aum_boot_state_efi, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (AUM_TYPE_BOOT_STATE, aum_boot_state_efi_iface_init));


/**
 * @brief Destroy environment structure
 *
 * @param state
 */
static void
aum_boot_state_efi_cleanup(AumBootStateEfi *state)
{
}

static gboolean
aum_boot_state_efi_is_active (AumBootState *boot_state)
{
  AumBootStateEfi *state = AUM_BOOT_STATE_EFI(boot_state);
  return state->active;
}

static gboolean
aum_boot_state_efi_read_dir(AumBootStateEfi *state)
{
  g_autoptr (GError) error=NULL;
  gchar *pattern = NULL;
  g_autoptr (GDir) dir_name = NULL;
  gchar *tries_done = NULL;
  const gchar *conf_file_name_ = NULL;

  dir_name = g_dir_open(DIR_NAME, 0, &error);
  if (!dir_name)
  {
    g_debug("Directory doesn't exist %s", __FUNCTION__);
    return FALSE;
  }

  state->update_available = FALSE;
  while ((conf_file_name_ = g_dir_read_name(dir_name)))
  {
    if (g_str_has_suffix(conf_file_name_, ".conf"))
    {
      pattern = g_strrstr(conf_file_name_, "+");
      if (pattern)
      {
        state->update_available = TRUE;
        tries_done = g_strstr_len(pattern, -1, "-");
        if (tries_done)
        {
          state->bootcount = atoi(tries_done + 1);
          g_debug("bootcount value is %d", state->bootcount);
          return TRUE;
        }
      }
    }
  }
  g_debug("Booted Successfully");
  state->bootcount = 0;
  return TRUE;
}

/**
 * @brief renaming the conf file for bootcounting mechanism.
 *
 * @param state
 *
 * @return
 */
static gboolean
aum_boot_state_efi_conf_file_save(AumBootStateEfi *state)
{
  g_autoptr (GDir) dir_name = NULL;
  g_debug("function name %s and bootlimit value %d", __FUNCTION__, state->bootlimit);
  g_autoptr (GFile) file_name = NULL, renamed_file = NULL;
  g_autoptr (GError) error=NULL;
  g_autofree gchar *renaming = NULL;
  g_auto(GStrv) options = NULL;
  const gchar *conf_file_name = NULL;

  dir_name = g_dir_open(DIR_NAME, 0, &error);
  if (!dir_name)
  {
    g_critical("error while opening the directory %s", error->message);
    return FALSE;
  }

  while ((conf_file_name = g_dir_read_name(dir_name)))
  {
    if (g_str_has_suffix(conf_file_name, "-2.conf"))
    {
      file_name = g_file_new_for_path(g_strconcat (DIR_NAME, conf_file_name, NULL));
      g_debug("Filename is  %s", g_file_peek_path (file_name));
      options = g_strsplit(conf_file_name, ".", 0);
      g_debug("options value is %s \n", options[0]);
      renaming = g_strdup_printf("%s+%d.%s", options[0], state->bootlimit, options[1]);
      renamed_file = g_file_set_display_name (file_name, renaming, NULL, &error);
      if (!renamed_file)
      {
        g_critical("error while renaming %s", error ? error->message : "");
        return FALSE;
      }
      g_debug("renaming of file was successful %s \n", g_file_peek_path (renamed_file));
      return TRUE;
    }
  }

  g_debug ("failed to rename EFI conf file\n");
  return FALSE;
}

static void
aum_boot_state_efi_init (AumBootStateEfi * state)
{
  state->active = FALSE;
  if (aum_boot_state_efi_read_dir(state) == FALSE)
    goto err;

  state->active = TRUE;

  g_message ("Loaded boot count from conf file: count %d",
             state->bootcount);
  return;

err:
  g_message ("Cannot initialize boot-state from EFI environment");
}

static const gchar *
aum_boot_state_efi_get_name (AumBootState * boot_state)
{
  return "EFI based boot";
}

static gint
aum_boot_state_efi_get_boot_count (AumBootState * boot_state)
{
  AumBootStateEfi *state = AUM_BOOT_STATE_EFI (boot_state);
  return state->bootcount;
}

static gint
aum_boot_state_efi_get_boot_limit (AumBootState * boot_state)
{
  AumBootStateEfi *state = AUM_BOOT_STATE_EFI (boot_state);
  return  state->bootlimit;
}

/**
 * @brief set the bootlimit value
 *
 * @param state, guint
 */

static void
aum_boot_state_efi_set_boot_limit (AumBootState * boot_state, guint limit)
{
  AumBootStateEfi *state = AUM_BOOT_STATE_EFI (boot_state);
  state->bootlimit = limit;
}

static gboolean
aum_boot_state_efi_get_update_available (AumBootState *boot_state)
{
  AumBootStateEfi *state = AUM_BOOT_STATE_EFI (boot_state);
  return state->update_available;
}

/**
 * @brief Trigger bootcount mechanism in EFI
 *
 * @param boot_state
 * @param update_available: set to true if an update has just been applied, and before rebooting, or false if the reboot was successful
 *
 * @return
 */
static gboolean
aum_boot_state_efi_set_update_available (AumBootState * boot_state,
                                               gboolean update_available)
{
  AumBootStateEfi *state = AUM_BOOT_STATE_EFI (boot_state);
  state->bootcount = 0;
  state->update_available = update_available;
  return aum_boot_state_efi_conf_file_save (state);
}

static void
aum_boot_state_efi_iface_init (AumBootStateInterface *iface)
{
  iface->is_active = aum_boot_state_efi_is_active;
  iface->get_name = aum_boot_state_efi_get_name;
  iface->get_boot_count = aum_boot_state_efi_get_boot_count;
  iface->get_boot_limit = aum_boot_state_efi_get_boot_limit;
  iface->set_boot_limit = aum_boot_state_efi_set_boot_limit;
  iface->get_update_available = aum_boot_state_efi_get_update_available;
  iface->set_update_available = aum_boot_state_efi_set_update_available;
}

static void
aum_boot_state_efi_dispose(GObject *obj)
{
  AumBootStateEfi *state = AUM_BOOT_STATE_EFI (obj);
  aum_boot_state_efi_cleanup(state);
}

static void
aum_boot_state_efi_class_init (AumBootStateEfiClass * cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);
  object_class->dispose = aum_boot_state_efi_dispose;
}
