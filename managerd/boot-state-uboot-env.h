/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2018 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2.1+
 */

/* inclusion guard */
#ifndef __AUM_BOOT_STATE_UBOOT_ENV_H__
#define __AUM_BOOT_STATE_UBOOT_ENV_H__

#include "boot-state.h"

G_BEGIN_DECLS

#define AUM_TYPE_BOOT_STATE_UBOOT_ENV (aum_boot_state_uboot_env_get_type ())

G_DECLARE_FINAL_TYPE (AumBootStateUbootEnv,
                      aum_boot_state_uboot_env,
                      AUM,
                      BOOT_STATE_UBOOT_ENV,
                      GObject);

G_END_DECLS

#endif /* __AUM_BOOT_STATE_UBOOT_ENV_H__ */
